import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { FirstPage } from '../first/first';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController) {

  }

  clickThisButton(){
    console.log("Click this button");
  }

  goToFirstPage(){
    console.log("Go to first page");
    this.navCtrl.push(FirstPage, {nama: 'andre', umur: '21'});
  }
}
